function getRuleS(url) {
    let d = [];
    try {
        d = fetch(url)
        if (url.startsWith('hiker://page/'))
            d = JSON.parse(d).rule
        d = JSON.parse(d)
    } catch (e) {
        d = []
    }
    return d.filter((v) => { return v.title != MY_RULE.title && v.author != '轻合集生成器' })
}

let url='https://codeberg.org/52fhy/PublicRule/raw/branch/master/fhyys.json';
var myurls=[
    'https://codeberg.org/52fhy/PublicRule/raw/branch/master/fhyys.json'
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=450384733' //晓
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=510381049' //逐风者
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=2579949378' //蓝莓
    ,'http://hiker.nokia.press/hikerule/dev/json_list?name=184462840' //回忆
];

function batchGet(urls){
    if (urls == undefined) {urls = myurls;}
    let d=[];
    for(url of urls){
        getRuleS(url).map(v=>{d.push(v)});
    }
    return d;
}

function getYum(){
    var t = JSON.parse(fetch('https://codeberg.org/52fhy/PublicRule/raw/branch/master/yum'));
    let d = [];
    d.push({
        title: '公告',
        url: 'toast://我怀疑你在搞事情',
        col_type: 'text_1'
    });
    for(var i=0;i<t.notice.length;i++)
    {
    var r={};
    var k=t.notice[i];      
    r.title=i+1+'.'+ k['title'];
    r.url='没事不要瞎点';
    r.col_type='text_1';
    d.push(r);
    }
    d.push({
        title: '小程序',
        url: 'toast://点击即可导入小程序',
        col_type: 'text_1'
    });
    for(var i=0;i<t.yum.length;i++)
    {
        var r={};
        var k=t.yum[i];      
        r.title=k['title'];
        r.url='rule://海阔视界规则分享，当前分享的是：小程序@' + base64Encode(JSON.stringify(k));
        r.col_type='text_3';
        d.push(r);
    }

    return d;
}

function getYumS(){
    let d = getYum();
    d.push({
        title: '杂货铺',
        url: 'toast://点击即可导入小程序',
        col_type: 'text_1'
    });
    let batchRule = batchGet(["https://codeberg.org/52fhy/PublicRule/raw/branch/master/fhywz.json"]);
    for(v of batchRule){
        var r={};   
        r.title=v['title'];
        r.url='rule://海阔视界规则分享，当前分享的是：小程序@' + base64Encode(JSON.stringify(v));
        r.col_type='text_3';
        d.push(r);
    }
    return d;
}
